package com.y2.transform.topology;

import com.y2.transform.model.GenericMsg;
import com.y2.transform.utils.JsonPOJODeserializer;
import com.y2.transform.utils.JsonPOJOSerializer;
import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.config.SslConfigs;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.kafka.streams.Consumed;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Produced;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;

@Service
public class KsTopologyBuilderImpl implements KsTopologyBuilder {

    @Value("${bootstrap.servers}")
    private String bootstrapServers;

    private Serde<Map<String, Object>> msgSerde;

    @Value("${elastic.out.topic}")
    private String elasticOutTopic;
    @Value("${input.topic}")
    private String inputTopic;

    private Properties streamsConfiguration;

    @PostConstruct
    public void init() {
        prepareConfiguration();
        prepareSerializers();
        deployTransformTopology();
    }

    @Override
    public void deployTransformTopology() {
        StreamsBuilder builder = new StreamsBuilder();

        KStream<String, Map<String, Object>> msgsStream = builder.stream(inputTopic, Consumed.with(Serdes.String(), msgSerde));
        msgsStream.to(elasticOutTopic, Produced.with(Serdes.String(), msgSerde));

        KafkaStreams transformTopology = new KafkaStreams(builder.build(), streamsConfiguration);
        transformTopology.start();
    }

    private void prepareSerializers() {
        Map<String, Object> serdeProps = new HashMap<>();

        final Serializer<Map<String, Object>> msgSerializer = new JsonPOJOSerializer<>();
        serdeProps.put("JsonPOJOClass", Map.class);
        msgSerializer.configure(serdeProps, false);

        final Deserializer<Map<String, Object>> msgDeserializer = new JsonPOJODeserializer<>();
        serdeProps.put("JsonPOJOClass", Map.class);
        msgDeserializer.configure(serdeProps, false);

        msgSerde = Serdes.serdeFrom(msgSerializer, msgDeserializer);
    }

    private void prepareConfiguration() {
        streamsConfiguration = new Properties();
        streamsConfiguration.put(StreamsConfig.APPLICATION_ID_CONFIG, "transfrom");
        streamsConfiguration.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        streamsConfiguration.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        streamsConfiguration.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        streamsConfiguration.put(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, 5 * 1000);
        streamsConfiguration.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");
    }

}
