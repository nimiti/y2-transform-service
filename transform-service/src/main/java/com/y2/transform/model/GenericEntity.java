package com.y2.transform.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class GenericEntity implements Serializable {
    private String action;
    private Map<String, Object> data = new HashMap<>();
    // private String data;
    private String uuid;

    // private ObjectMapper om;

/*    public void setObjectMapper(ObjectMapper om) {
        this.om = om;
    }*/

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }

 /*   public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }*/

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
/*
    public String getJsonData() {
        String json = null;
        if (om != null) {
            try {
                json = om.writeValueAsString(data);
                System.out.println(json);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
        return json;
    }*/

    @Override
    public String toString() {
        return "GenericEntity{" +
                "action='" + action + '\'' +
                ", data='" + data + '\'' +
                ", uuid='" + uuid + '\'' +
                '}';
    }
}
